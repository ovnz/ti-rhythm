# ti-RHYTHM
Rhythm game written in TI-83 Basic, should work on any monochrome system from the [TI-83 family of calculators](http://tibasicdev.wikidot.com/thecalcs) (will probably work on colour editions as well, but it was not designed with those systems in mind).

The code was written for the [TokenIDE](http://www.ticalc.org/archives/files/fileinfo/433/43315.html) editor.

### Gameplay
The notechart will be parsed at runtime, and displayed once the UI has been drawn. Before playing the chart, you must play 1 measure of 4th notes for the game to compute at which BPM you're playing (the remaining 4th notes are indicated by 4 pixels at the top left of the screen).

After having played the full chart, the game will display when your key presses occured within the chart, so that you can compare your timing with the position of the notes. Then, the calculator will be in a *Pause* state and will display error statistics in 16th division time after getting out of it.

### Notechart editing
The notechart is stored in a string at the very beginning of the program.

Each 4th division is separated by a `,`, and every `1` (note) and `0` (silence) will be arranged **evenly** within that division. There is a maximum of 20 total divisions (the parser will ignore any further data).

### Building
*soon*